# Reto Liverpool Ejercicio 2

Resumen: Reto Liverpool (Ejercicio 1) búsqueda de productos. 
Esta aplicación muestra un sitio web de comercio electrónico. La aplicación puede mostrar
productos desde base de datos MongoDB o utilizar una API externa (API Liverpool). Los usuarios pueden seleccionar productos ingresar para obtener más detalles, incluyendo precios, opiniones y calificación. Los usuarios pueden seleccionar productos y agregarlos a su carrito de compras

## Instalación

Para empezar, simplemente hay que clonar este repositorio e instalar las dependencias.


>git clone 

>npm install

>npm start

## Tecnologías
* AngularJS
* Node.js
* Express.js
* Bootstrap
* ui-Router
