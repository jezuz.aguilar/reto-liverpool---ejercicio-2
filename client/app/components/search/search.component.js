const searchController = function($scope, databaseService, $location) {
    $scope.queryString = $location.search().query;

    loadSearchItems();

    function loadSearchItems() {
        databaseService.getFromDatabase(`https://www.liverpool.com.mx/tienda/?s=${$scope.queryString}&d3106047a194921c01969dfdec083925=json`)
            .then((items) => {
                $scope.items = items;
            });
    }
};

angular.module('myApp')
    .component('mySearch', {
        controller: searchController,
        templateUrl: 'app/components/search/search.html'
    })
    .config(function($stateProvider) {
        $stateProvider
            .state('search', {
                url: '/search',
                component: 'mySearch'
            });
    });